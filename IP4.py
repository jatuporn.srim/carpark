from flask import Flask , request,redirect,url_for
from flask  import  render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import text
from flask_restplus import Resource, Api, fields

app = Flask(__name__, template_folder='./templates')

SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="pock9tal",
    password="may123456",
    hostname="pock9tal.mysql.pythonanywhere-services.com",
    databasename="pock9tal$listname",
)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Listname(db.Model):
    __tablename__ =  "list"
    id = db.Column(db.Integer, primary_key=True)
    Fname = db.Column(db.String(30))
    Lnamd = db.Column(db.String(30))
    phone = db.Column(db.String(11))
    Nocar = db.Column(db.String(10))

@app.route('/')
def showdata():
    data = Listname.query.all()
    return render_template("index.html",data=data)


@app.route("/insert", methods=["POST"])
def insert():
    fname = request.form.get("fname")
    lname = request.form.get("lname")
    phone = request.form.get("phone")
    NOcar = request.form.get("nocar")
    listcus = Listname(Fname=fname,Lnamd=lname,phone=phone,Nocar=NOcar)
    db.session.add(listcus)
    db.session.commit()
    return redirect(url_for("showdata"))






if __name__ == '__main__':
    app.run(app, debug=True)
